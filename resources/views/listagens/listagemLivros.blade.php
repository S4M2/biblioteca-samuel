@extends('templates.template_principal')
@section('title')
Livros
@endsection
@section('content')
<div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Livro</th>
                <th scope="col">Id</th>
                <th scope="col">Id do Autor</th>
                <th scope="col">Id da Editora</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($livros as $livros)
            <tr>
                <th scope="row">{{$livros->id}}</th>
                <td>{{$livros->livro}}</td>
                <td>{{$livros->id}}</td>
                <td>{{$livros->id_autor}}</td>
                <td>{{$livros->id_editora}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection