@extends('templates.template_principal')
@section('title')
Editar Livros
@endsection
@section('content')
<div>
    <button type="button" class="btn btn-outline-success" style="margin: 10px;">Adicionar</button>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Livro</th>
                <th scope="col">Id</th>
                <th scope="col">Id do Autor</th>
                <th scope="col">Id da Editora</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($livros as $livros)
            <tr>
                <th scope="row">{{$livros->id}}</th>
                <td>{{$livros->livro}}</td>
                <td>{{$livros->id}}</td>
                <td>{{$livros->id_autor}}</td>
                <td>{{$livros->id_editora}}</td>
                <td><a href="/editar_livros/{{$livros->id}}"><button type="button" class="btn btn-outline-warning"
                            style="margin-right: 5px;">Editar</button></a>
                    <a href=""><button type="button" class="btn btn-outline-danger">Excluir</button></a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
