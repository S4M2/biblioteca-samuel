@extends('templates.template_principal')
@section('title')
Editar Autores
@endsection
@section('content')
<div>
    <button type="button" class="btn btn-outline-success" style="margin: 10px;">Adicionar</button>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Autor</th>
                <th scope="col">Id</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($autores as $autores)
            <tr>
                <th scope="row">{{$autores->id}}</th>
                <td>{{$autores->autores}}</td>
                <td>{{$autores->id}}</td>
                <td><a href="/editar_autor/{{$autores->id}}"><button type="button" id="{{$autores->id}}" class="btn btn-outline-warning btEditar"
                            style="margin-right: 5px;">Editar</button></a>
                    <a href=""><button type="button" class="btn btn-outline-danger">Excluir</button></a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
@section('scripts')
<script>
$('.btEditar').click(function(){
    var id = $(this).attr('id');
    var url = "{{url('/')"+'/editar_autor/'+id;
    //alert(url);
    window.location.href = url;
});
</script>
@endsection
