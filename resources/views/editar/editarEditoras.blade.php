@extends('templates.template_principal')
@section('title')
Editar Editoras
@endsection
@section('content')
<div>
    <button type="button" class="btn btn-outline-success" style="margin: 10px;">Adicionar</button>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Editora</th>
                <th scope="col">Id</th>
                <th scope="col"></th>

            </tr>
        </thead>
        <tbody>
            @foreach ($editoras as $editoras)
            <tr>
                <th scope="row">{{$editoras->id}}</th>
                <td>{{$editoras->editoras}}</td>
                <td>{{$editoras->id}}</td>
                <td><a href="/editar_editora/{{$editoras->id}}"><button type="button" class="btn btn-outline-warning"
                            style="margin-right: 5px;">Editar</button></a>
                    <a href=""><button type="button" class="btn btn-outline-danger">Excluir</button></a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
