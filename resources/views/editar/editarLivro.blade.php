@extends('templates.template_principal')
@section('title')
Editar Livros
@endsection
@section('content')
<div class="row">
        <div class="col-md titulo">
            <h2 class="nome_titulo">Editar {{$livros->livro}}</h2>
        </div>
    </div>

    <form action="{{url('salva_livro')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$livros->id}}">
        <div class="form-group">
            <h4 class="label_nome">Nome do livro:</h4>
            <input type="text" class="form-control inputtxt" id="formGroupExampleInput" name="nome" value="{{$livros->nome}}">
        </div>
        <button type="submit" class="btn btn-success btnsubmit">Salvar alterações</button>
    </form>

    </div>
    @endsection