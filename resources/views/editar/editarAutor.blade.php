@extends('templates.template_principal')
@section('title')
Editar Autores
@endsection
@section('content')
<div class="row">
        <div class="col-md titulo">
            <h2 class="nome_titulo">Editar {{$autor->autores}}</h2>
        </div>
    </div>

    <form action="{{url('salva_autor')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$autor->id}}">
        <div class="form-group">
            <h4 class="label_nome">Nome do autor:</h4>
            <input type="text" class="form-control inputtxt" id="formGroupExampleInput" name="nome" value="{{$autor->nome}}">
        </div>
        <button type="submit" class="btn btn-success btnsubmit">Salvar alterações</button>
    </form>

    </div>
    @endsection