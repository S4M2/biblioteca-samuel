@extends('templates.template_principal')
@section('title')
Editar Editoras
@endsection
@section('content')
<div class="row">
        <div class="col-md titulo">
            <h2 class="nome_titulo">Editar {{$editoras->editoras}}</h2>
        </div>
    </div>

    <form action="{{url('salva_editora')}}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{$editoras->id}}">
        <div class="form-group">
            <h4 class="label_nome">Nome da editora:</h4>
            <input type="text" class="form-control inputtxt" id="formGroupExampleInput" name="nome" value="{{$editoras->nome}}">
        </div>
        <button type="submit" class="btn btn-success btnsubmit">Salvar alterações</button>
    </form>

    </div>
    @endsection