<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link
        href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <title>@yield('title')</title>

</head>

<body>
    <h1 style="text-align: center;">Biblioteca - Samuel dos Anjos</h1>
    <div class="blocos">

        <div class="d-grid gap-2 d-md-block" style="margin-bottom: 10px; text-align:center">
            <a href="/biblioteca/livros"><button type="button" class="btn btn-outline-primary">Livros</button></a>
            <a href="/biblioteca/autores"><button type="button" class="btn btn-outline-primary">Autores</button></a>
            <a href="/biblioteca/editoras"><button type="button" class="btn btn-outline-primary">Editoras</button></a>
        </div>
        <div class="d-grid gap-2 d-md-block" style="text-align:center">
            <a href="/biblioteca/editalivros"><button type="button" class="btn btn-outline-primary">Editar
                    Livros</button></a>
            <a href="/biblioteca/editaautores"><button type="button" class="btn btn-outline-primary">Editar
                    Autores</button></a>
            <a href="/biblioteca/editaeditoras"><button type="button" class="btn btn-outline-primary">Editar
                    Editoras</button></a>
            <br><br>

            @yield('content')

</body>
<footer style="text-align: center;">
    <a href="/biblioteca"><button type="button" style="margin-top: 20%" class="btn btn-outline-info">Voltar ao
            Início</button></a>
</footer>

</html>
