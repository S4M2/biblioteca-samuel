<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link
        href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <style>
        .botao {
            text-align: center;
        }
    </style>
    <title>Biblioteca</title>

</head>

<body>
    <div style="text-align: center;">
        <h1 id="titulo">Biblioteca - Samuel dos Anjos</h1>
    </div>
    <div class="botao">

        <a href="/biblioteca/livros"><button type="button" style="font-size: 30px; margin-top: 10%;" class="btn btn-outline-primary" id="btmenu">Acessar Biblioteca</button></a>

    </div>
</body>

<footer style="text-align: center;">
    <a href="/laravel"><button type="button" style="margin-top: 20%" class="btn btn-outline-info">Welcome</button></a>
</footer>

</html>