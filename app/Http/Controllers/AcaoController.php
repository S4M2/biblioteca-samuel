<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Autores;

use App\Models\Livros;

use App\Models\Editoras;

class AcaoController extends Controller
{
    public function passadados(){

        $livros=Livros::all();
        $autores=Autores::all();
        $editoras=Editoras::all();
        
    return view('startPage', ['livros'=>$livros, 'autores'=>$autores, 'editoras'=>$editoras]);
    }

    public function mostralivros(){

        $livros=Livros::all();
        $autores=Autores::all();
        $editoras=Editoras::all();
        
    return view('listagens/listagemLivros', ['livros'=>$livros, 'autores'=>$autores, 'editoras'=>$editoras]);
    }
    public function mostraautores(){

        $livros=Livros::all();
        $autores=Autores::all();
        $editoras=Editoras::all();
        
    return view('listagens/listagemAutores', ['livros'=>$livros, 'autores'=>$autores, 'editoras'=>$editoras]);
    }
    public function mostraeditoras(){

        $livros=Livros::all();
        $autores=Autores::all();
        $editoras=Editoras::all();
        
    return view('listagens/listagemEditoras', ['livros'=>$livros, 'autores'=>$autores, 'editoras'=>$editoras]);
    }
    public function laravelpage(){

        $livros=Livros::all();
        $autores=Autores::all();
        $editoras=Editoras::all();
        
    return view('welcome', ['livros'=>$livros, 'autores'=>$autores, 'editoras'=>$editoras]);
    }
//Editar Livros-------------------------------------------------------------------------------
public function editarlivros(){
    $livros = \App\Models\Livros::get();
    return view('editar/editarLivros')->with(compact('livros'));
}
public function editar_livros($id){
    $livros = \App\Models\Livros::find($id);
    if($livros){
        return view('editar/editarLivro',['livros'=>$livros]);
    } else {
        alert('Erro ao acessar lançamento!');
        return redirect()->back()->withInput();
}
}
public function salva_livro(Request $request){
    $livros = \App\Models\Livros::find($request->id);
    $livros->livro = $request->nome;
    $livros->save();
    return redirect('/biblioteca/editalivros');
}
//Editar Autores-------------------------------------------------------------------------------
    public function editarautores(){
        $autores = \App\Models\Autores::get();
        return view('editar/editarAutores')->with(compact('autores'));
    }
    public function editar_autor($id){
        $autor = \App\Models\Autores::find($id);
        if($autor){
            return view('editar/editarAutor',['autor'=>$autor]);
        } else {
            alert('Erro ao acessar lançamento!');
            return redirect()->back()->withInput();
    }
}
    public function salva_autor(Request $request){
        $autor = \App\Models\Autores::find($request->id);
        $autor->autores = $request->nome;
        $autor->save();
        return redirect('/biblioteca/editaautores');
    }
//Editar Editoras-------------------------------------------------------------------------------
public function editareditoras(){
    $editoras = \App\Models\Editoras::get();
    return view('editar/editarEditoras')->with(compact('editoras'));
}
public function editar_editora($id){
    $editoras = \App\Models\Editoras::find($id);
    if($editoras){
        return view('editar/editarEditora',['editoras'=>$editoras]);
    } else {
        alert('Erro ao acessar lançamento!');
        return redirect()->back()->withInput();
}
}
public function salva_editora(Request $request){
    $editoras = \App\Models\Editoras::find($request->id);
    $editoras->editoras = $request->nome;
    $editoras->save();
    return redirect('/biblioteca/editaeditoras');
}
    
}
