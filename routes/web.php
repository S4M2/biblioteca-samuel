<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Http\Controllers\AcaoController;


Route::get('/biblioteca', [AcaoController::class, 'passadados']);

Route::get('/biblioteca/livros', [AcaoController::class, 'mostralivros']);
Route::get('/biblioteca/autores', [AcaoController::class, 'mostraautores']);
Route::get('/biblioteca/editoras', [AcaoController::class, 'mostraeditoras']);

Route::get('/laravel', [AcaoController::class, 'laravelpage']);

Route::get('/biblioteca/editalivros', [AcaoController::class, 'editarlivros']);
Route::get('/editar_livros/{id}', [AcaoController::class, 'editar_livros']);
Route::post('/salva_livro', [AcaoController::class, 'salva_livro']);

Route::get('/biblioteca/editaautores', [AcaoController::class, 'editarautores']);
Route::get('/editar_autor/{id}', [AcaoController::class, 'editar_autor']);
Route::post('/salva_autor', [AcaoController::class, 'salva_autor']);

Route::get('/biblioteca/editaeditoras', [AcaoController::class, 'editareditoras']);
Route::get('/editar_editora/{id}', [AcaoController::class, 'editar_editora']);
Route::post('/salva_editora', [AcaoController::class, 'salva_editora']);

